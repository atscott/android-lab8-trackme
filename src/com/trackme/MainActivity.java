package com.trackme;

import android.app.Activity;
import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.widget.TextView;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class MainActivity extends Activity {
    private GoogleMap googleMap;
    private Marker currentMarker;
    private final LocationListener locationListener = new LocationListener() {
        @Override
        public void onLocationChanged(Location location) {
            updateWithNewLocation(location);
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {

        }

        @Override
        public void onProviderEnabled(String s) {
        }

        @Override
        public void onProviderDisabled(String s) {
        }
    };


    LocationManager locationManager;
    Criteria criteria;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        googleMap = ((MapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();

        String svcName = Context.LOCATION_SERVICE;
        locationManager = (LocationManager) getSystemService(svcName);
        criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        criteria.setPowerRequirement(Criteria.POWER_LOW);
        criteria.setAltitudeRequired(false);
        criteria.setBearingRequired(false);
        criteria.setSpeedRequired(false);
        criteria.setCostAllowed(true);
        String provider = locationManager.getBestProvider(criteria, true);
        Location l = locationManager.getLastKnownLocation(provider);
        updateWithNewLocation(l);
    }

    private void updateWithNewLocation(Location location) {
        if (currentMarker != null) {
            currentMarker.remove();
        }
        if (location != null) {
            final LatLng CIU = new LatLng(location.getLatitude(), location.getLongitude());
            currentMarker = googleMap.addMarker(new MarkerOptions().position(CIU).title("You're here!"));
            googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition(CIU, 13, 0, 0)));
            TextView mapLabel = (TextView) findViewById(R.id.mapLabel);
            mapLabel.setText(String.format("Latitude: %1$s\nLongitude:%2$s", location.getLatitude(), location.getLongitude()));
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        String provider = locationManager.getBestProvider(criteria, true);
        locationManager.requestLocationUpdates(provider, 2000, 10, locationListener);
    }

    @Override
    public void onDestroy() {
        locationManager.removeUpdates(locationListener);
    }
}
